# Bash Scripting for DH Data Handling

**Charles H. Pence**  
*Last Updated: [see the list of commits](https://github.com/cpence/bash-scripting-dh/commits/master)*

One of the most common tasks that one has to accomplish in digital humanities projects is to move data around and parse it at the command line. And the most common standard set of tools for this is the basic Unix command-line system, available in Mac OS X and Linux without much further work.

This is my best effort to write up a tutorial covering some of the most common operations that you might have to do. I'm going to often assume that you have at least some familiarity with basic programming-language concepts – things like variables, loops, strings, and basic syntax.

Also, this repository contains three text files in the `sample-data` folder up above -- download those and do all your work in a folder containing them if you want your results to look like mine.

## Table of Contents

| By section:    | By command: |
| -------------- | ----------- |
| [Saving scripts, basics, and comments](#saving-scripts-basics-and-comments) | [chmod](#saving-scripts-basics-and-comments) |
| [Operating on lists of files](#operating-on-lists-of-files-for-loops-and-variables) | [cut](#parsing-csv-files-cut) |
| [Searching through text: `grep`](#searching-through-text-grep) | [grep](#searching-through-text-grep) |
| [Output streams and echoing](#output-streams-and-echoing) | [head](#trimming-output-head-and-tail) |
| [Looping over command output](#looping-over-command-output) | [tail](#trimming-output-head-and-tail) |
| [Standard input and piping](#standard-input-and-piping) ||
| [An Example: Filtering Files](#an-example-filtering-files) ||
| [Trimming output: `head` and `tail`](#trimming-output-head-and-tail) ||
| [Parsing CSV files: `cut`](#parsing-csv-files-cut) ||
| [License and other details](#license) ||



## Saving scripts, basics, and comments

Of course, because your terminal runs the shell, you can do almost all of this at the command prompt without saving your scripts. But you'll regret it. So whenever you want to do anything remotely complex, you'll want to create a new script. Create a file with the extension `.sh`, preferably with a descriptive name -- e.g., `rename-gutenberg-xml.sh`, something that you *might* be able to understand when you come back to it later. Into your new script, put the following:

```bash
#!/bin/bash
# That first line, up above, is the 'shebang' or 'hashbang', which will tell
# the shell that you're running a shell script.
#
# The rest of these lines starting with hash signs are comments. USE COMMENTS!
# If you comment a script, you might be able to understand it again later. I
# recommend starting every script with a block of comments like this that
# reminds you what it's for and any command-line arguments that it takes.

# The 'echo' command will print anything you send it to the console. You can
# surround strings with single or double quotes. There is a distinction between
# them, which we'll talk about later.
echo 'Hello world!'
```

For future script excerpts, I won't include the hashbang, and may not include any comments if I've described what I'm doing in the surrounding text. But you should.

> Technical aside: You'll note that I'm explicitly referring to the `bash` shell in the above hashbang. There are a number of shells that can execute basic shell scripts, and which have small variations between them (`bash`, `zsh`, `ash`, `dash`, etc.). Almost every system has bash installed, though, and it has a couple of fancy features that you might make use of.

Now, you'll want to know how to run the script file you just created. First, you have to set an internal flag that tells the operating system "hey, this text file is actually a program that you can run." To do that, use the `chmod` or "change mode" program:

```
> chmod +x my_script
```

The `+x` parameter tells `chmod` to add the "executable" mode to the file you specify. Now you can run the script simply by typing:

```
> ./my_script
```

Note that to run it, you have to specify that it sits in the current directory, by prefixing the filename with `./`. This is a security measure -- you want to be *sure* you're running scripts in the current directory only on purpose. (Imagine if someone maliciously put a script named `ls` or `mv` in your home directory!)


## Operating on lists of files: for loops and variables

One of the most common data-management tasks is to perform some manipulation over a set of files in a directory. One of the basic primitives in bash is a for loop, just as often appears in many programming languages. The basic form of a for loop (over a list of files) is:

```bash
for doc in *.txt
do
    echo "$doc"
done
```

This script will print out the name of every text file in the current directory. A few things here that are worthy of note.

*   The `doc` in the for loop creates a new variable, which will then later be referenced as `$doc`.
*   I strongly recommend you name your for-loop indices with reasonable names rather than `$i` (as you often would in C/C++). Trust me, it makes things much more readable.
*   **Important:** If you're looping over a list of files, you *must* enclose the filename variable in double-quotes every time you use it. If you do not, your script will begin to fail in all sorts of confusing and problematic ways every time you use filenames with spaces in them. (Alternatively, you can just *never* put spaces in your filenames. I try not to whenever I possibly can.)

To give you a better idea of how variables work in Bash, there's a few things you should know. Unlike in many other programming languages, variables do not have types -- all variables are, essentially, strings, although there are a number of commands that will manipulate a variable containing a number as though it were a number (for mathematical calculation, say). To declare your own variable from scratch, write it at the start of a line, with an equal sign:

```bash
new_variable="this is my new variable"

echo $new_variable
```

Again, unlike in many programming languages, you do not have to declare variables in advance prior to using them. Any value that was already present in `$new_variable` above would simply be (silently) overwritten when you set the variable's new value. Also note that you *do not* need a dollar-sign when declaring a new variable (the equals-sign in that line of code makes it clear to Bash that what you're doing is declaring a variable), but you *do* need a dollar-sign in essentially every case in which you're planning to *use* a variable.

This is also a good time to mention the difference between single and double quotes in shell scripts. Run the following script to check it out:

```bash
for doc in *.txt
do
    echo "$doc"
    echo '$doc'
done
```

You'll find that for each text file in the current directory, first the script will print out the filename, then it will print out the literal string `$doc`. Strings inside double quotes will have variables within them expanded, strings inside single quotes will not. So if you mean to use a variable in a string, make sure it's in double quotes – and if you don't mean to use a variable, enclose your string in single quotes so you don't accidentally make a mistake with a dollar-sign.

This is already useful, in and of itself – for example, if you'd like to run another command on every one of a variety of files, you can do that:

```bash
for doc in *.txt
do
    my_command "$doc"
done
```

This is also a great way to *chain scripts together* – very often, you'll want to build your scripts as small, incremental bits that you can wire up together in complex ways to solve larger problems. There is an even more powerful method than this, to which we'll return in a bit. But first you need something to chain together, so let's look at a file search tool.


## Searching through text: `grep`

The `grep` tool is a fantastic way to search through large numbers of text files. It is exceptionally powerful, given that it can make use of *regular expressions,* a very (!) flexible search vocabulary for matching strings of text. Those are a bit beyond the scope of this document, but [you can find a great interactive tutorial for them here.](https://regexone.com/)

For now, we can just use them to search through text files for basic strings. For example, try:

```bash
grep -F 'purpose' *.txt
```

to find the string 'purpose' in any of the text files in the current folder. The `-F` command-line parameter sets `grep` into a mode that will *not* use regular expressions (remove it if you're practicing your regular expression matching). Output will look something like this (if you `grep` for the string 'purpose':

```
> grep -F purpose *
emma.txt:the parlour one night on purpose to sing to her. She was very fond
emma.txt:“Perhaps you think I am come on purpose to quarrel with you, knowing
emma.txt:purpose, for she found her decidedly more sensible than before of Mr.
...
```

(Depending on your console settings, you might even get fancy colored output here.) You can see that you get the filename (if you searched through more than one file), then a colon, then the line on which the string appears. Also note that `grep` is finding the *string* you provide, not the *word* – so here it gladly returned (if you scroll down a bit) a match for 'purposely'. If you really want to find the *word* purpose, you might search for `' purpose '` (note the spaces on either side) or, to use a little regular expression syntax, `grep 'purpose\s'`, where `\s` is the regular expression syntax to match any kind of whitespace (a space, a tab, or a line separator).

If you'd like more context than this, you can set the `-B` and `-A` parameters to control the number of lines printed before and after the match, which will return output like:

```
> grep -B 2 -A 2 -F purpose *
emma.txt-some walnuts, because she had said how fond she was of them, and in
emma.txt-every thing else he was so very obliging. He had his shepherd's son into
emma.txt:the parlour one night on purpose to sing to her. She was very fond
emma.txt-of singing. He could sing a little himself. She believed he was very
emma.txt-clever, and understood every thing. He had a very fine flock, and, while
--
emma.txt-Knightley.”
emma.txt-
emma.txt:“Perhaps you think I am come on purpose to quarrel with you, knowing
emma.txt-Weston to be out, and that you must still fight your own battle.”
emma.txt-
--
...
```

Now it's time to take the output from `grep` and do something with it.


## Output streams and echoing

It's time to really learn how to connect these small parts of scripts together, which will require us to get into a bit more depth about inputs and outputs. To start, let's see how you take the output from a command and save it into a variable in a script. Go back to your script, and add the following:

```bash
matches=`grep -F 'purpose' *.txt`
echo 'Grep is done running'
echo $matches
```

The backwards quotation marks (or backticks, the key just to the left of 1 on a standard US keyboard layout) indicate that the shell should *capture* the output from that command, and assign the matches variable to it. The script will print 'Grep is done running' *before* it prints the output from `grep`, as it's captured Grep's terminal output rather than displaying it.

Every Unix command actually has *two* output streams -- "standard output" and "standard error." The former is the command's actual output, and it's what will be captured by this command. If `grep` finds any errors, they *won't* be captured, as standard error will just be printed out to the terminal. (For example, try adding the invalid argument `--notanargument` to your `grep` command line above. It will print the error messages -- as they are not captured -- then the line 'Grep is done running', and then nothing, as there was no output produced, just an error message.)

You can use this to your advantage when you write scripts. By default, `echo` prints to standard output. But you can print to standard error by writing:

```bash
echo 'This is an error message' >&2
```

That way you can reproduce `grep`'s behavior in your own scripts -- printing output to standard error (so that you could capture the output from your scripts and use it in *other* scripts), and sending error messages to the user on standard error.


## Looping over command output

Once you have captured the output from a command, you can parse it in a variety of ways. By default, if you pass a variable containing text to a bash for-loop, it will just loop over every word, separated by spaces. So running:

```bash
matches=`grep -F purpose *.txt`
for word in $matches
do
  echo "Next piece is: $word"
done
```

will produce as output:

```
Next piece is: emma.txt:the
Next piece is: parlour
Next piece is: one
Next piece is: night
Next piece is: on
Next piece is: purpose
Next piece is: to
Next piece is: sing
Next piece is: to
Next piece is: her.
Next piece is: She
...
```

If you would like to split by lines rather than by words, you can do that, too -- and it's often very useful for cases like `grep` where output is line-by-line. Running:

```bash
IFS='
'

matches=`grep -F purpose *.txt`
for line in $matches
do
  echo "Next piece is: $line"
done
```

returns each line rather than each word.

Note that we're setting the internal bash variable `$IFS` (the "inter-field separator") to a string *containing a newline character* (see that the string starts on one line and ends on the next line).

That script will produce:

```
Next piece is: emma.txt:the parlour one night on purpose to sing to her. She was very fond
Next piece is: emma.txt:“Perhaps you think I am come on purpose to quarrel with you, knowing
Next piece is: emma.txt:purpose, for she found her decidedly more sensible than before of Mr.
Next piece is: emma.txt:own civility, to leave Mr. Knightley for that purpose. Mr. Knightley,
Next piece is: emma.txt:the Abbey two evenings ago, on purpose to consult me about it. He knows
...
```


## Standard input and piping

One more concept needs to be mentioned -- in addition to standard output and error, there is also a standard *input* stream. We'll talk about how to read from it later, but for now, know that many Unix commands that can take filenames can also have their input provided through standard input.

To see this, first let's introduce another command: `cat`. This is one of the simplest commands you'll learn. `cat <file>` simply prints the contents of `<file>` to standard output.

So, for example, the following two commands will produce the same output:

```bash
grep -F 'purpose' emma.txt
cat emma.txt | grep -F 'purpose'
```

Note that there is *no* filename argument to `grep`, and `grep` thus knows to search through whatever is coming in on its standard input. The pipe character (`|`, shift-backslash on a US keyboard) "pipes together" the output from the command on the left to the input of the command on the right. So, as we know, `cat` will take the contents of `emma.txt` and print them on standard output. That standard *output* stream from `cat` (aka `emma.txt`) will then be used as the standard *input* stream for `grep` -- which is why these two commands just do the same thing.


## An Example: Filtering Files

Here's another way in which you can put together everything you've learned so far. Imagine you have a folder of text files, and you want to move all files containing "Smith" to another folder. You could do that with the following script:

```bash
IFS='
'

matches=`grep -l -F Smith *.txt`
for filename in $matches
do
  echo "Filename is: $filename"
  mv "$filename" out-folder
done
```

This script will move all files containing "Smith" to the folder `out-folder`. The command-line parameter `-l`, when passed to `grep`, makes `grep` print only the name of every file containing a match to the given search string. In this case, therefore, `grep` prints the name of every file containing "Smith". We then loop over those filenames, and move all of them to the folder `out-folder`.


## Trimming output: `head` and `tail`

Now that you know all about piping, you can get two of the most useful tools for quickly looking at the output of complex commands -- `head` and `tail`. As you might predict, these print the first few lines or the last few lines (the default is 10) of standard input to standard output. So, for example, `grep -F purpose emma.txt | head` will show you the first ten matches in that file, and `grep -F purpose emma.txt | tail` will show you the last ten.

And you can build chains of pipes that are as long as you want! So:

```bash
cat emma.txt | grep -F purpose | head
```

is another way to get the first ten matches, while

```bash
cat emma.txt | grep -F purpose | head | tail -n 2
```

will give you matches #9 and 10 from the file (the last two of the first ten matches). You can pass the argument `-n <number>` to either head or tail to specifically set the number of lines that `head` or `tail` returns.

If you want to run a variable into a chain of commands, `echo` has always just been printing the contents of its arguments to standard output -- so you could write:

```bash
matches=`grep -F purpose emma.txt`
echo $matches | head | tail -n 2
```

as yet another way of achieving the same result.


## Parsing CSV files: `cut`

Let's start to apply this in context. With one more command, we'll be able to parse CSV files! Let's learn about `cut`.

The `cut` command will take its input, separate it by some delimiter, and return some of the fields thus separated. To see the basic idea, try the following command:

```
> echo 'one,two,three,four' | cut -d',' -f3
three
```

As you can see here, the `-d` string parameter to `cut` will tell it what the delimiter for each line is – what separates each field. The `-f` parameter tells `cut` which field to return (indexed, unlike many commands, starting with 1 rather than 0).

To combine much of what's gone before, imagine that you have a CSV file with the path to a file in the first column. Now, say that you want to move the files specified in that CSV file to a new folder. You could do that as follows (with sample data in the repository):

```bash
IFS='
'

mkdir out-folder

for line in `cat spreadsheet.csv`
do
  echo "Line: $line"
  filename=`echo $line | cut -d',' -f1`
  echo "Filename: $filename"
  mv $filename out-folder
done
```

This script, then, reads each line of `spreadsheet.csv`, takes the filename from the first column, and then moves each file there to the folder named `out-folder`.


## License

This tutorial is copyright [Charles H. Pence,](https://charlespence.net/) 2018, and released under CC-BY 4.0. <https://creativecommons.org/licenses/by/4.0/>
